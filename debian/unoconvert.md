% unoconvert(1) | User Commands
%
% "May  2 2024"

# NAME

unoconvert - LibreOffice document conversion client

# SYNOPSIS

**unoconvert** [**\-\-convert-to CONVERT_TO**] [**\-\-input-filter INPUT_FILTER**]
               [**\-\-output-filter OUTPUT_FILTER**] [**\-\-filter-options FILTER_OPTIONS**]
               [**\-\-update-index**] [**\-\-dont-update-index**] [**\-\-host HOST**] [**\-\-port PORT**]
               [**\-\-host-location** {**auto**,**remote**,**local**}] _infile_ _outfile_

**unoconvert** [{**-h** | **\-\-help**} | {**-v** | **\-\-version**}]

# DESCRIPTION

This manual page documents briefly the **unoconvert** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page.

**unoconvert** is a program that converts LibreOffice documents using a running
**unoserver**(1).

# OPTIONS

_infile_
: The path to the file to be converted (use **-** for stdin)

_outfile_
: The path to the converted file (use **-** for stdout)

**\-\-convert-to**:
The file type/extension of the output file (ex **pdf**). Required when using stdout

**\-\-input-filter**
:   The LibreOffice input filter to use (ex **writer8**), if autodetect fails

**\-\-output-filter**
:   The export filter to use when converting. It is selected automatically if not specified.

**\-\-filter**
:   Deprecated alias for **\-\-output-filter**

**\-\-filter-option**
:   Pass an option for the export filter, in name=value format.
    Use **true**/**false** for boolean values. Can be repeated for multiple options.

**\-\-filter-options**
:   Deprecated alias for **\-\-filter-option**.

**\-\-host**
:   The host used by the server, defaults to "127.0.0.1"

**\-\-port**
:   The port used by the server, defaults to "2002"

**\-\-host-location**
:   The host location determines the handling of files. If you run the client on the
    same machine as the server, it can be set to local, and the files are sent as paths. If they are
    different machines, it is remote and the files are sent as binary data. Default is auto, and it will
    send the file as a path if the host is 127.0.0.1 or localhost, and binary data for other hosts.

**-v**, **\-\-version**
:   Display version and exit.

# EXAMPLE

Example for setting PNG width/height:

    unoconvert infile.odt outfile.png --filter-options PixelWidth=640 --filter-options PixelHeight=480

# SEE ALSO

**libreoffice**(1), **unoserver**(1), **unocompare**(1)

# AUTHOR

Colomban Wendling <cwendling@hypra.fr>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2024 Colomban Wendling

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
