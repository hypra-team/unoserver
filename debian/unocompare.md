% unocompare(1) | User Commands
%
% "May  2 2024"

# NAME

unocompare - LibreOffice document comparison client

# SYNOPSIS

**unocompare** [**\-\-file-type FILE_TYPE**] [**\-\-host HOST**]
               [**\-\-port PORT**]
               [**--host-location** {**auto**,**remote**,**local**}]
               _oldfile_ _newfile_ _outfile_

**unocompare** [{**-h** | **\-\-help**} | {**-v** | **\-\-version**}]

# DESCRIPTION

This manual page documents briefly the **unocompare** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page.

**unocompare** is a program that compares LibreOffice documents using a running
**unoserver**(1).

# OPTIONS


_oldfile_
:   The path to the older file to be compared with the original one (use **-** for stdin)

_newfile_
:   The path to the newer file to be compared with the modified one (use **-** for stdin)

_outfile_
:   The path to the result of the comparison and converted file (use **-** for stdout)

**--file-type**
:   The file type/extension of the result output file (ex **pdf**). Required when using stdout

**--host**
:   The host used by the server, defaults to "127.0.0.1"

**--port**
:   The port used by the server, defaults to "2002"

**--host-location**
:   The host location determines the handling of files. If you run the client on the
    same machine as the server, it can be set to local, and the files are sent as paths. If they are
    different machines, it is remote and the files are sent as binary data. Default is auto, and it will
    send the file as a path if the host is 127.0.0.1 or localhost, and binary data for other hosts.

**-v**, **\-\-version**
:   Display version and exit.

# SEE ALSO

**libreoffice**(1), **unoserver**(1), **unoconvert**(1)

# AUTHOR

Colomban Wendling <cwendling@hypra.fr>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2024 Colomban Wendling

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
