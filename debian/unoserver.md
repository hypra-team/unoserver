% unoserver(1) | User Commands
%
% "May  2 2024"

# NAME

unoserver - LibreOffice document conversion server

# SYNOPSIS

**unoserver** [**\-\-interface INTERFACE**] [**\-\-uno-interface UNO_INTERFACE**]
              [**\-\-port PORT**] [**\-\-uno-port UNO_PORT**] [**\-\-daemon**]
              [**\-\-executable EXECUTABLE**]
              [**\-\-user-installation USER_INSTALLATION**]
              [**\-\-libreoffice-pid-file LIBREOFFICE_PID_FILE**]

**unoserver** [{**-h** | **\-\-help**} | {**-v** | **\-\-version**}]

# DESCRIPTION

This manual page documents briefly the **unoserver** command.

This manual page was written for the Debian distribution because the
original program does not have a manual page.

**unoserver** is a LibreOffice document conversion server designed to be used
through the **unoconvert**(1) and **unocompare**(1) client tools.

# OPTIONS

**\-\-interface**
:   The interface used by the XMLRPC server, defaults to "127.0.0.1"

**\-\-port**
:   The port used by the XMLRPC server, defaults to "2003"

**\-\-uno-interface**
:   The interface used by the LibreOffice server, defaults to "127.0.0.1"

**\-\-uno-port**
:   The port used by the LibreOffice server, defaults to "2002"

**\-\-daemon**
:   Daemonize the server

**\-\-executable**
: The path to the LibreOffice executable

**\-\-user-installation**
:   The path to the LibreOffice user profile, defaults to a dynamically created
    temporary directory

**\-\-libreoffice-pid-file**
:   If set, unoserver will write the Libreoffice PID to this file.
    If started in daemon mode, the file will not be deleted when unoserver
    exits.

**-v**, **\-\-version**
:   Display version and exit.

# SEE ALSO

**libreoffice**(1), **unoconvert**(1), **unocompare**(1)

# AUTHOR

Colomban Wendling <cwendling@hypra.fr>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2024 Colomban Wendling

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
